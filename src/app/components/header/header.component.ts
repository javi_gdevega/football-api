import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public imageLogo: string = 'https://i.pinimg.com/736x/8d/43/ff/8d43ffcf11f2a9e1c5f47e5c2aaa6df0.jpg';
  public altLogo: string = 'Football logo';
  constructor() { }

  ngOnInit(): void {
  }

}
