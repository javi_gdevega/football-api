export interface TeamApiResponse {
    idTeam: string;
    details: TeamResponse[];
}

export interface TeamResponse {
    strTeam: string;
    strLeague: string;
    strStadium: string;
    strRSS: string;
    strStadiumThumb: string;
    strStadiumDescription: string;
    strStadiumLocation: string;
    strStadiumCapacity: string;
    strWebsite: string;
    strFacebook: string;
    strTwitter: string;
    strInstagram: string;
}
export interface TeamFormattedResponse {
    strTeam: string;
    strLeague: string;
    strStadium: string;
    strStadiumThumb: string;
    strStadiumDescription: string;
    strStadiumLocation: string;
    strStadiumCapacity: string;
    strFacebook: string;
    strTwitter: string;
    strInstagram: string;
}


export interface Iplayer {
    idPlayer: string;
    strPlayer: string;
    strTeam: string;
    strNationality: string;
    dateBorn: string;
    strWage: string;
    strBirthLocation: string;
    strDescriptionEN: string;
}
export interface Iteam {
    strTeam: string;
    strLeague: string;
    strStadium: string;
    strRSS: string;
    strStadiumThumb: string;
    strStadiumDescription: string;
    strStadiumLocation: string;
    strStadiumCapacity: string;
    strWebsite: string;
    strFacebook: string;
    strTwitter: string;
    strInstagram: string;
}
