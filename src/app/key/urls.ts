export const URL = {
    url: 'https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=Arsenal',
    urlTeam: 'https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=Arsenal',
    urlPlayer: 'https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?p=Danny%20Welbeck',
    urlStats: 'https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=Arsenal_vs_Chelsea',
}