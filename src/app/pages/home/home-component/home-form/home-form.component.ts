
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Iform } from './models/iform';


@Component({
  selector: 'app-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.scss']
})
export class HomeFormComponent implements OnInit {
  public teamForm: FormGroup = null;
  // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) {
    this.teamForm = this.formBuilder.group(
      {
        team: ['', [Validators.required]]
      });
   }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    // El usuario ha pulsado en submit->cambia a true submitted
    this.submitted = true;
    // Si el formulario es valido
    if (this.teamForm.valid) {
      // Creamos un Usuario y lo emitimos
      const user: Iform = {
        team: this.teamForm.get('team').value,
      };
      console.log(user);
      // Reseteamos todos los campos y el indicador de envío o submitted
      this.teamForm.reset();
      this.submitted = false;
    }
  }
}
