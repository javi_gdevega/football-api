import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { URL } from './../../key/urls';
import { Iplayer, TeamResponse, Iteam, TeamFormattedResponse, TeamApiResponse } from './../../models/model';


@Injectable({
  providedIn: 'root'
})
export class FootballService {
  private teamURL = URL.urlTeam;
  public teamList: TeamFormattedResponse[] = [];
  private playerURL = URL.urlPlayer;
  private statsURL = URL.urlStats;
  constructor(private http: HttpClient) {
    /*empty*/}

  public getTeams(): Observable<TeamFormattedResponse[]> {
        return this.http.get(this.teamURL).pipe(map((response: TeamApiResponse) => {
            if (!response) {
                throw new Error ('Value expected!');
            } else {
                const formattedResults: TeamFormattedResponse[] = response.results.map(({
                    strTeam, strLeague, strStadium, strStadiumThumb, strStadiumDescription, strStadiumLocation, strStadiumCapacity, strFacebook, strTwitter, strInstagram }) => ({
                        strTeam,
                        strLeague,
                        strStadium,
                        strStadiumThumb,
                        strStadiumDescription,
                        strStadiumLocation,
                        strStadiumCapacity,
                        strFacebook,
                        strTwitter,
                        strInstagram,
                    })
                );
                this.teamList = formattedResults;
                console.log(this.teamList);
                return formattedResults;
            }
        }),
        catchError((err) => {
        throw new Error(err.message);
        })
    );
  }
}
